/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Vivek
 */
public class ParkingPersonWorkRequest extends WorkRequest{
    
    private Double parkingamount;

    public Double getParkingamount() {
        return parkingamount;
    }

    public void setParkingamount(Double parkingamount) {
        this.parkingamount = parkingamount;
    }

    @Override
    public String toString() {
        return String.valueOf(parkingamount) ;
    }
    
    
}
